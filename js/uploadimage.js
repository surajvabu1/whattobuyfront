/**
 * Created by sghimire on 2/22/2016.
 */

(function( $ ){

    $.fn.uploadImage = function() {

        return this.each(function() {

            // create html elements

            // imagelist of images as unordered imagelist
            $imagelist = $('<ul id="imageListContainer"/>');
            var $input = $('<input type="file">').change(function(){
                // input
//            var $input = $('<input type="file" />').keyup(function(event) {
//
//                if(event.which == 32 || event.which == 188) {
//                    // key press is space or comma
                var val = $(this).val();//.replace(/C:\\fakepath\\/i, '')//.slice(0, -1); // remove space/comma from value

                // append to imagelist of images with remove button
                $imagelist.append($('<li class="uploadimage-images"><span class="imagesList">' + val + '</span></li>')
                        .append($('<a href="#" class="uploadimage-close" title="Remove" />')
                            .click(function(e) {
                                $(this).parent().remove();
                                e.preventDefault();
                            })
                    )
                );
                $(this).attr('placeholder', '');
                // empty input
                $(this).val('');
//                }
            });

            // container div
            var $imagecontainer = $('<div class="uploadimage-container" />').click(function() {
                $input.focus();
            });

            // insert elements into DOM
            $imagecontainer.append($imagelist).append($input).insertAfter($(this));

            // add onsubmit handler to parent form to copy images into original input as csv before submitting
            var $orig = $(this);
            $(this).closest('form').submit(function(e) {

                var images = [];
                $('.uploadimage-images span').each(function() {
                    images.push($(this).html());
                });
                images.push($input.val());
                $orig.val(images);

////                $orig.val(images.join());
////               var tr_array = $('.uploadimage-images span').map(function() {
////
////                   $orig.val($input.val());
////                   return tr_array;
////                });
            });

            return $(this).hide();

        });

    };
})( jQuery );

//function addColorToEdit(images) {
//
//
//    var imagesList = images.split(",");
//
//
//    var fc = $("#imageListContainer").firstChild;
//
//    while( fc ) {
//        $("#imageListContainer").removeChild( fc );
//        fc = $("#imageListContainer").firstChild;
//    }
//
//    for(image in imagesList) {
//        if (imagesList[image] != "") {
//            $("#imageListContainer").append($('<li class="uploadimage-images"><span class="imagesList">' + imagesList[image] + '</span></li>')
//                    .append($('<a href="#" class="uploadimage-close" title="Remove" />')
//                        .click(function (e) {
//                            $(this).parent().remove();
//                            e.preventDefault();
//                        })
//                )
//            );
//        }
//    }
//}
