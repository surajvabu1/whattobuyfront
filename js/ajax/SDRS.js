if(typeof SDR !== 'object'){
    var SDR = {};
}

(function (){

    'use strict';

    var requestHeaders = {"Accept":"application/json", "Content-Type":"application/json"};

    function request(url, request_type, datatype,callback,postdata){

        // For debug only
        var SERVER = "http://localhost:8080"; /*"http://52.27.46.230:8080";*/
        url = SERVER+url;
        console.log(url);
        if(typeof(postdata) == "object"){
            postdata = JSON.stringify(postdata);

        }

        $.ajax({
            beforeSend: function(xhr){
                for(var key in requestHeaders){
                    xhr.setRequestHeader(key, requestHeaders[key]);
                    // ajaxindicatorstart("...Please wait.. Data is loading...");
                }
            },
            url: url,
            xhrFields: {
                withCredentials: true
            },
            data: postdata,
            dataType: datatype,
            type: request_type,
            mimeType: "text/plain; charset=x-user-defined",
            success: function(data) {
               // ajaxindicatorstop();
                callback("success", data);
            },
            error: function(xhr, status, error) {
               // ajaxindicatorstop();
                try{
                    var errorMessageObj = JSON.parse(xhr.responseText);
                }catch(err){
                    errorMessageObj = {type:"ERROR", message:xhr.responseText};
                }
                callback(error, errorMessageObj);
            }
        });
    }

// ---------------------------------- Member Details ------------------------------------------
    SDR.register = function(firstName,lastName,email,password,confirmPassword,address,phoneNumber,callback) {
    var url = "/whattobuyback/registration/register";

        alert (firstName +" "+ lastName +" "+ email+" "+ password + " "+ confirmPassword + " " + address +" " + phoneNumber);

    var registerData = {
        firstName:firstName,
        lastName:lastName,
        email:email,
        password:password,
        confirmPassword:confirmPassword,
        address:address,
        phoneNumber:phoneNumber
    };

    request(url,"POST","json",callback,registerData);
};

    SDR.insertbankdetails = function(cardNumber, accountName, securityCode, issueDate, expiryDate, cardType, callback){
        var url = "/whattobuyback/registration/bankDetails";

        alert (cardNumber +" "+ accountName+" "+ securityCode + " "+ issueDate + " " + expiryDate +" " + cardType);

        var bankdetails = {
            cardNumber: cardNumber,
            accountName: accountName,
            securityCode: securityCode,
            issueDate: issueDate,
            expiryDate: expiryDate,
            cardType: cardType
        };

        request(url,"POST", "json",callback, bankdetails);
    };

    // ----------------------------------- Member Management Section ----------------------------------
    SDR.viewMemberDetails = function(callback) {
        var url = "/whattobuyback/userManagement/viewMemberDetails";

        request(url,"GET","json",callback);
    };

    SDR.updateCustomer = function(id,firstName,lastName,email,password,confirmPassword,address,phoneNumber,callback){
        var url= "/whattobuyback/userManagement/updateCustomer";
        alert (firstName +" "+ lastName +" "+ email+" "+ password + " "+ confirmPassword + " " + address +" " + phoneNumber);
        var update = {
            id:id,
            firstName:firstName,
            lastName:lastName,
            email:email,
            password:password,
            confirmPassword:confirmPassword,
            address:address,
            phoneNumber:phoneNumber
        };
        request(url,"POST","json",callback,update);
    };

    SDR.viewBankDetails = function(callback) {
        var url = "/whattobuyback/userManagement/viewBankDetails";

        request(url,"GET","json",callback);
    };

    SDR.updateBankDetails = function(cardNumber,accountName,securityCode,issueDate,expiryDate,cardType,callback){
        var url= "/whattobuyback/userManagement/updateBankDetails";
        alert (cardNumber +" "+ accountName+" "+ securityCode + " "+ issueDate + " " + expiryDate +" " + cardType);
        var update = {
            cardNumber:cardNumber,
            accountName:accountName,
            securityCode:securityCode,
            issueDate:issueDate,
            expiryDate:expiryDate,
            cardType:cardType
        };
        request(url,"POST","json",callback,update);
    };

// ----------------------------------- Product Management Section ----------------------------------

//    SDR.addProduct = function(categoryId,productName, productPrice, productDescription, productColor, productQuantity, productImage,callback) {
//        var url = "/whattobuyback/productManagement/addProduct";
//        var productDetails = {
//            categoryId : categoryId,
//            productName : productName,
//            productPrice : productPrice,
//            productDescription : productDescription,
//            productColor : productColor,
//            productQuantity : productQuantity,
//            productImage : productImage
//        };
//
//        request(url,"POST","json",callback,productDetails);
//    };

    SDR.viewDetail = function(productId,callback){
        var url = "/whattobuyback/productManagement/viewProduct?"+
            "productId="+productId;

        request(url,"GET","json",callback);
    };

    SDR.getProductInformation = function(callback) {
        var url = "/whattobuyback/productManagement/productInformation?";
        request(url,"GET","json",callback);
    };

    SDR.saveCategory = function (categoryPrimary, categorySecondary, categoryDescription, callback) {
        var url = "/whattobuyback/categoryManagement/saveCategory";
        alert (categoryPrimary + " " + categorySecondary +" "+categoryDescription);
        var categoryDetails = {
            categoryPrimary : categoryPrimary,
            categorySecondary : categorySecondary,
            categoryDescription : categoryDescription
        };

        request(url,"POST","json",callback,categoryDetails);
    };

    SDR.getCategoryList = function(callback) {
        var url = "/whattobuyback/categoryManagement/categoryList";
        request(url,"POST","json",callback);
    };

    SDR.getProductList = function(callback) {
        var url = "/whattobuyback/productManagement/productList";
        request(url,"POST","json",callback);
    };

    SDR.deleteProduct = function(id,callback) {
        var url = "/whattobuyback/productManagement/deleteProduct?" +
            "id="+id;
        request(url,"GET","json",callback);
    };

    SDR.login = function(email,password,callback) {
        var url = "/whattobuyback/registration/login";
        var memberDetails = {
        email: email,
        password: password
    };
// +
//        "email="+email+
//        "&password="+password;
    request(url,"POST","json",callback, memberDetails);
};

SDR.logout = function(callback) {
    var url = "/whattobuyback/registration/logout";
    request(url,"POST","json",callback);
};

}());