/**
 * Created by sghimire on 12/17/2015.
 */

// ------------------------- Cookie Management Section -------------------------
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

function checkCookie(name) {
    var user = getCookie(name);
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie(name, user, 365);
        }
    }
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

// ---------------------------------- Member Details ------------------------------------------
function register(){
    var firstName = $("#firstName").val();
    var lastName = $("#lastName").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var confirmPassword = $("#confirmPassword").val();
    var address = $("#address").val();
    var phoneNumber = $("#phoneNumber").val();

    SDR.register(firstName,lastName,email,password,confirmPassword,address,phoneNumber,registerfunc());

}

function registerfunc(){
    return function(status, data){
        if(data.connection == "success") {
            window.location="account.html";
        }
    }
}

function insertbankdetails() {
    var cardNumber = $("#cardNumber").val();
    var accountName = $("#accountName").val();
    var securityCode = $("#securityCode").val();
    var issueDate = $("#issueDate").val();
    var expiryDate = $("#expiryDate").val();
    var cardType = $("#cardType").val();

    SDR.insertbankdetails(cardNumber,accountName,securityCode,issueDate,expiryDate,cardType, insertbankdetailsfunc());
}

function insertbankdetailsfunc(){
    return function(status, data){
        alert(JSON.stringify(data))
        if(data.connection == "success"){
            window.location= "success.html";
        }
    }
}

// ----------------------------------- Member Management Section ----------------------------------

function viewMemberDetails() {

    SDR.viewMemberDetails(memberDetailsCallback());
}

function memberDetailsCallback(){
    return function(status, data){
        alert(JSON.stringify(data))
        if(data.connection == "success"){
            $("#identity").val(data.identity);
            $("#firstName").val(data.firstName);
            $("#lastName").val(data.lastName);
            $("#email").val(data.email);
            $("#password").val(data.password);
            $("#confirmPassword").val(data.confirmPassword);
            $("#address").val(data.address);
            $("#phoneNumber").val(data.phoneNumber);
        }
    }
}

function viewBankDetails(){
    SDR.viewBankDetails(bankDetailsCallback());
}

function bankDetailsCallback(){
    return function(status, data){
        if(data.connection == "success"){
            $("#cardNumber").val(data.cardNumber);
            $("#accountName").val(data.accountName);
            $("#securityCode").val(data.securityCode);
            $("#issueDate").val(data.issueDate);
            $("#expiryDate").val(data.expiryDate);
            $("#cardType").val(data.cardType);
        }
    }
}

function updateCustomer(firstName, lastName, email, password, confirmPassword, address, phoneNumber) {
    var id = $("#identity").val();
    if(id != "")
        SDR.updateCustomer(id,firstName, lastName, email, password, confirmPassword, address, phoneNumber, updateCustomerCallback());
}

function updateCustomerCallback(){
    return function(status,data){
        if(data.connection = "success"){
            alert(JSON.stringify(data))
            $("#firstName").value(firstName);
            $("#lastName").val(lastName);
            $("#email").val(email);
            $("#password").val(password);
            $("#confirmPassword").val(confirmPassword);
            $("#address").val(address);
            $("#phoneNumber").val(phoneNumber);

            //location.reload();
        }
    }
}

function updateBankDetails(cardNumber, accountName, securityCode, issueDate, expiryDate, cardType) {
        SDR.updateBankDetails(cardNumber, accountName, securityCode, issueDate, expiryDate, cardType, updateBankDetailsCallback());
}

function updateBankDetailsCallback(){
    return function(status,data){
        if(data.connection = "success"){
            alert(JSON.stringify(data))
            $("#cardNumber").val(cardNumber);
            $("#accountName").val(accountName);
            $("#securityCode").val(securityCode);
            $("#issueDate").val(issueDate);
            $("#expiryDate").val(expiryDate);
            $("#cardType").val(cardType);

           location.reload();
        }
    }
}

function login(){
    var email = $("#username").val();
    var password = $("#userPassword").val();

    SDR.login(email,password,loginfunc());

}

function loginfunc(){
    return function(status, data){
        alert(JSON.stringify(data));
        if(data.connection == "success") {
            if(data.role == "Customer"){
                setCookie("whattobuyname",data.name,1);
                window.location="index.html";
            }
            if(data.role == "Admin"){
                window.location="admin.html"
            }
        }
    }
}

// ----------------------------------- Product Management Section ----------------------------------

function addProduct(){
    var categoryId = $("#categoryList").val().split("_")[0];
    var categoryName = $("#categoryList").val().split("_")[1];
    var productName = $("#productName").val();
    var productPrice = $("#productPrice").val();
    var productDescription = $("#productDescription").val();
    var productColor = "";
    var elements = document.getElementsByClassName("colorList");
    for (var i = 0, len = elements.length; i < len; i++) {
        if(i==len-1) {
            productColor+=elements[i].innerHTML+"]";
        }
        else if(i==0) {
            productColor+="["+elements[i].innerHTML+",";
        }
        else {
            productColor+=elements[i].innerHTML+",";

        }

    }
    var productQuantity = $("#productQuantity").val();
    var productImage = $("#productImage");

    SDR.addProduct(categoryId,productName,productPrice,productDescription,productColor,productQuantity,productImage,addProductCallback());
}

function addProductCallback(){
    return function(status,data){
        alert(JSON.stringify(data));
        if(data.connection == "success") {
            $("#myModel").modal("show");
        }else if(data.connection == "error") {
            $("#myErrorModel").modal("show");
        }
    }
}

function viewDetail(){
    var productId = $("#productList").val().split("_")[0];

//    $.ajax({
//        url: "http://localhost:8080/whattobuyback/productManagement/viewImage?productId="+productId,
//        type: "GET",
//        xhrFields: {
//            withCredentials: true
//        },
//        mimeType: "text/plain; charset=x-user-defined"
//    }).success(function(data) {
//
//        var test = data.imgvalue;
//        console.log(test);
//        $("#image").attr('src', 'data:image/jpeg;base64,' + test);
//    }).error(function(data) {
//        console.log(data);
//    });

    SDR.viewDetail(productId,viewDetailCallback());
}

function viewDetailCallback(){
    return function(status, data){
       //alert(JSON.stringify(data));

            $("#category").html("<b>Category: </b>"+data.categoryBoth);
            $("#name").html("<b>Name: </b>"+data.productName);
            $("#price").html("<b>Price: </b>"+data.productPrice);
            $("#description").html("<b>Description: </b>"+data.productDescription);
            $("#color").html("<b>Color: </b>"+data.productColor);
            $("#quantity").html("<b>Quantity: </b>"+data.productQuantity);
            $("#picture").html("<b>Image:</b>");
            //var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
            var imageString = data.productImage;
            imageString = imageString.replace('[','').replace(']','');
            $("#image").attr("src",imageString);

            if($("#showDetail").class = "hidden") {
                $("#showDetail").removeClass("hidden");
            }
        }
}

function getProductInformation() {
    SDR.getProductInformation(productInformationCallback());
}
function productInformationCallback() {
    return function(status, data){
        tableData=[];
        for( index in data.data) {
            var product = data.data[index];

            var colors = product.color;
            var colorDisplay="";
            for(sequence in colors) {
                var color = colors[sequence];
                //console.log(color);
                if(sequence ==0) {
                    colorDisplay += color.split("_")[0]+",";
                }
                else if(sequence == colors.length -1) {
                    colorDisplay += color.split("_")[0];
                }
                else {
                    colorDisplay += color.split("_")[0]+",";
                }
            }
            var edit =  "<div class='span2'><button onclick='deleteProduct(\""+product.id+"\")' class='btn btn-sm btn-danger btn-block'>Delete</button><button class='btn btn-sm btn-primary btn-block' onclick='editProduct(\""+product.categoryId+"\",\""+product.id+"\",\""+product.productName+"\",\""+product.productPrice+"\",\""+product.productDescription+"\",\""+colorDisplay+"\",\""+product.productQuantity+"\",\""+product.productImage+"\")' >Edit</button></div>";
            var imageString = product.productImage;
            imageString = imageString.replace('[','').replace(']','');
            tableData.push([product.categoryId.split("_")[1], product.productName,product.productPrice,product.productDescription,colorDisplay,product.productQuantity,'<div class="thumb-image"><img id="place-holder-1" src='+imageString+' data-imagezoom="true" class="img-responsive" alt="" /></div>',edit]);

        }
        createDataTable("productInformation",tableData,0,"asc");
    }
}

function deleteProduct(id) {
    $("#deleteModal").modal("show");
    $('#deleteModal').on('click',"#delete", function() {
        SDR.deleteProduct(id, deleteProductCallback());
    });
    $('#deleteModal').on('click',"#cancel", function() {
        location.reload();
    });
}
function deleteProductCallback() {
    return function(status, data) {
        location.reload();
        if(data.connection == "success") {
            getProductInformation();
        }
    }
}

function editProduct(id, categoryId, productName, productPrice, productDescription, colors, productQuantity, productImage){
    $("#manageDetail").addClass("hidden");
    $("#addDetail").removeClass("hidden");
    //alert(categoryId +" "+ productName+" "+ productPrice+" "+ productDescription+" "+ colors+" "+ productQuantity+" "+ productImage);

    $("#categoryList").selectedIndex = categoryId;//.split("_")[1];

    $("#productName").val(productName);
    $("#productPrice").val(productPrice);
    $("#productDescription").val(productDescription);
    addColorToEdit(colors,"colour");
    $("#productQuantity").selectedIndex = productQuantity;
    var imageString = productImage;
    imageString = imageString.replace('[','').replace(']','');
    $("#base64Value").val(imageString);
    $("#uploadedImage").attr("src",imageString);
    $("#identity").val(id);
}

function saveCategory(){
    var categoryPrimary = $("#categoryPrimary").val();
    var categorySecondary = $("#categorySecondary").val();
    var categoryDescription = $("#categoryDescription").val();

    SDR.saveCategory(categoryPrimary,categorySecondary,categoryDescription,saveCategoryCallback());
}

function saveCategoryCallback(){
    return function(status, data){
        alert (JSON.stringify(data))
        if(data.connection == "success"){
            $("#myModel").modal("show");
            $("#categoryPrimary").val("");
            $("#categorySecondary").val("");
            $("#categoryDescription").val("");
        }else if(data.connection == "failed") {
            $("#myErrorModel").modal("show");
        }
    }
}

function categoryList(selectItemId) {
    SDR.getCategoryList(categoryListCallback(selectItemId));
}
function categoryListCallback(selectItemId) {
    return function(status, data) {
        //alert(JSON.stringify(data))
        for(index in data.data) {
            var category = data.data[index];
            $("#"+selectItemId).append(new Option(category.categoryPrimary+"-"+category.categorySecondary, category.id+"_"+category.categoryPrimary));
        }
//        $("#"+selectItemId).multiselect();
    }
}

function productList(selectItemId) {
    SDR.getProductList(productListCallback(selectItemId));
}
function productListCallback(selectItemId) {
    return function(status, data) {
        //alert(JSON.stringify(data))
        for(index in data.data) {
            var product = data.data[index];
            $("#"+selectItemId).append(new Option(product.productName, product.id+"_"+product.productName));
        }
//        $("#"+selectItemId).multiselect();
    }
}

function logout(){
    SDR.logout(logoutfunc());
}

function logoutfunc(){
    return function(status, data){
        if(data.connection == "success") {
            window.location="index.html";
        }
    }
}
